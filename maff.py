# %%
import math


def mean(*args):
    l = list(args)
    return sum(l) / len(l)


def median(*args):
    l = list(args)
    l.sort()
    i = math.floor(len(args) / 2)

    if(len(l) % 2 != 0):
        return l[i]

    x = l[i-1]
    y = l[i]
    return mean(x, y)


def find_missing_val(mean, *vals):
    l = list(vals)
    return mean * (len(l) + 1) - sum(l)


def iqr(*vals):
    l = list(vals)
    if(len(l) % 2 == 0):
        return


def mad(*vals):

    l = list(vals)
    m = mean(*l)
    devs = [abs(i - m) for i in l]

    return sum(devs) / len(devs)


def pre_tax(tax, rate):
    return tax/rate


def roll_dist(r, times):
    return 2*r*math.pi*times


def circ_area(r):
    return r**2*math.pi


def area(x, y):
    return x * y


def cub_area(x, y, z):
    return x*y*z


def p_model(*vals):
    l = list(vals)
    t = sum(l)
    print(t)

    return [i/t for i in vals]


# p_model(72,112,711)
# p_model(13,18,6)
print(p_model(97, 47, 77))
print(97/221, 47/221, 77/221)


# %%
def line():
    #
    return


def get_x(total, s1, o1, s2, o2):
    sl = s1 + s2
    os = o1 + o2
    # print(total,sl,os)
    x = (total - os) / sl
    return x


def sub_x(x, slope, offset):
    return slope * x + offset


def get_sx(s1, o1, s2, o2):
    return (o2 - o1) / (s1 - s2)


x = get_sx(5, -15, 2, 21)
sub_x(x, 2, 21)

# %%
